<?php


if(isset($_GET['id'])) {
    $user_id = $_GET['id'];
} else {
    die("Provide ID");
}

$url = 'http://54.204.46.226/Yoddle/scripts/get-single-user-by-id.php';
$ch = curl_init();
//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch,CURLOPT_POST, 1);
curl_setopt($ch,CURLOPT_POSTFIELDS, "user_id=$user_id");
//execute post
$result = curl_exec($ch);
//close connection
curl_close($ch);
