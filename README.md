# Security Breach Report By Shako Kakauridze #
## Test Subject: IOS APP YODDLE - CREATED BY [APPPARTNER](http://apppartner.com) ##
Test was intended to find vulnerability issues and security breaches in REST API of YODDLE APP.


# During this test found that, REST API was so unsecured that we could copy whole users database!!! #
You can find grabbed users database list [on this URL.](https://bitbucket.org/shako_kakauridze/yoddlesecuritybreach/src/43c2cce289c582bff24481bd93caccc3be811c7f/users_grabber/data/?at=master)

Below I will explain what should be done to handle this kind of breaches and vulnerabilities.

![Screen Shot 2015-10-29 at 20.37.06.png](https://bitbucket.org/repo/GG5kn9/images/2374735244-Screen%20Shot%202015-10-29%20at%2020.37.06.png)


# Here I will start step By Step what was done wrong in REST API implementation. #
1. Whole BackEnd Endpoints were exposed to everyone. This endpoints should be hidden for security breach. I will paste some of them below:
     * http://54.204.46.226/Yoddle/scripts/update-user-location.php -  If I pass user_id,lat,lng to this endpoint I can update location for other users. Without logging into system.
     * http://54.204.46.226/Yoddle/scripts/get-in-the-know.php
     * http://54.204.46.226/Yoddle/scripts/update-user-location.php
     * http://54.204.46.226/Yoddle/scripts/update-user-profile.php - This endpoint is exposed and I can change status for other users!!!!! Highly vulnerable breach.
     * http://54.204.46.226/Yoddle/scripts/get-single-user-by-id.php - If I pass user_id to this endpoint I get whole data about user including sensitive data email and etc...!!! Highly vulnerable!!!!


# Whole Authentication Layer was not done correctly and I could get data about other users emails and sensitive data! Some of them I paste below #

### User #4 ###

```
#!javascript
{
  "user_id": 4,
  "username": "greg.g",
  "email": "g.c.guidone@gmail.com",
  "facebook_id": "4713909"
}
```

### User #1 ###

```
#!javascript
{
  "user_id": 1,
  "username": "Bender1012",
  "email": "bender1012@gmail.com",
  "facebook_id": "14231865",
}
```
[Find All Users Data Here](https://bitbucket.org/shako_kakauridze/yoddlesecuritybreach/src/43c2cce289c582bff24481bd93caccc3be811c7f/users_grabber/data/?at=master)
Data is saved in JSON files with format **user_id.json**

# Communication between APP and REST was not encoded in SSL!!! #
http requests should be changed to https ones.


# REST API DIDN'T HAVE ANY hashed parameter to protect against vulnerable calls!!! #
These is good practice to send **hash=md5(params+secret)** on every request to identify that requester knows correct REST Security key before grant access on resources. If this was implemented in REST I would not be able to grab users sensitive data!!!


# Architecture of REST was not done correctly and every endpoint had different file this is not standard of creating REST endpoints #

### There were some other issues which I didn't write here. If you have questions I can advise any time. ###